# Background

Split a list of emails by company and limit each company to at most 5 contacts.

# Process

1. I sorted the list by email address domain, the part after the @ sign
2. If the number of contacts for an email domain is less than or equal to 5, all of them were included
3. Otherwise I took one C-level or VP level, one Director level, and the rest managers or individual contributors
4. When I had to pick a C-level, or directory level, I picked the lowest level:
a. I.e. give a choice of CIO, Senior VP or VP, I chose the VP
