package com.ciber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class Contact {

    static ArrayList<String> allHeaders = null;

    HashMap<String, String> contactData = new HashMap<>();
    public final static String REGION = "Region";
    public final static String EMAILDOMAIN = "EmailDomain";
    public final static String SENIORITYLEVEL = "Employee Seniority Level";
    public final static String SENIORITYSORTLEVEL = "Employee Seniority Sort Level";
    public final static String EMPLOYEEWORKEMAIL = "Employee Work Email";
    public final static String EMAILDOMAINREVERSE = "Email Domain Reverse";
    public final static String EMAILTOPLEVEL = "Email Top Level";

    private void putData(String key, String value) {
        contactData.put(key, value);
    }

    // only countrys with a .com under the country code
    // i.e. blah.co.uk, but not blah.ca
    // the UK uses a .co.uk but Canada use company name under .ca
    static HashMap<String, String> countryTLD;

    static HashMap<String, String> getCountryTLD() {
        if (countryTLD == null) {
            countryTLD = new HashMap<String, String>();
            countryTLD.put("uk", "foo");
            countryTLD.put("sg", "foo");
            countryTLD.put("mx", "foo");
            countryTLD.put("au", "foo");
            countryTLD.put("hk", "foo");
            countryTLD.put("cn", "foo");
            countryTLD.put("be", "foo");
            countryTLD.put("nz", "foo");
            countryTLD.put("br", "foo");
            countryTLD.put("in", "foo");
            countryTLD.put("tw", "foo");
            countryTLD.put("jp", "foo");
            countryTLD.put("za", "foo");
        }
        return countryTLD;
    }

    public static Contact getContact(String sheetName, List<String> headings, Row row) {
        Contact currentContact = new Contact();
        currentContact.putData(REGION, sheetName);

        for (int colNum = 0; colNum < headings.size(); colNum += 1) {
            String h = headings.get(colNum);
            Cell cell;
            try {
                cell = row.getCell(colNum);
            } catch (NullPointerException np) {
                return null;
            }

            String value = "";
            if (cell == null) {
                if (colNum == 0) {
                    return null;
                }
            } else {
                switch (cell.getCellType()) {
                case BLANK:
                    if (colNum == 0) {
                        return null;
                    }
                    break;
                case BOOLEAN:
                    boolean booleanValue = cell.getBooleanCellValue();
                    value = Boolean.toString(booleanValue);
                    break;
                case NUMERIC:
                    double number = cell.getNumericCellValue();
                    value = Double.toString(number);
                    break;

                default:
                    value = cell.getStringCellValue();
                    break;
                }
            }
            currentContact.putData(h, value);
        }

        saveAllHeaders(headings);

        return currentContact;
    }

    private static void saveAllHeaders(List<String> headings) {
        if (allHeaders == null) {
            allHeaders = new ArrayList<>();
            allHeaders.add(REGION);
            allHeaders.add(EMAILDOMAIN);
            allHeaders.add(EMAILDOMAINREVERSE);
            allHeaders.add(SENIORITYLEVEL);
            allHeaders.add(SENIORITYSORTLEVEL);
            allHeaders.add(EMPLOYEEWORKEMAIL);
            allHeaders.add(EMAILTOPLEVEL);
        }
        for (String h : headings) {
            if (!allHeaders.contains(h)) {
                allHeaders.add(h);
            }
        }

        if (!allHeaders.contains(EMAILDOMAIN)) {
            allHeaders.add(EMAILDOMAIN);
        }
        if (!allHeaders.contains(SENIORITYLEVEL)) {
            allHeaders.add(SENIORITYLEVEL);
        }
        if (!allHeaders.contains(SENIORITYSORTLEVEL)) {
            allHeaders.add(SENIORITYSORTLEVEL);
        }

    }

    public void printMe() {
        contactData.forEach((k, v) -> System.out.printf("%s -> %s \n", k, v));
    }

    public void createEmailDomain() {
        String emailString = contactData.get(EMPLOYEEWORKEMAIL).trim();

        String atDomain = getAtDomain(emailString);
        contactData.put(EMAILDOMAINREVERSE, new StringBuilder(atDomain).reverse().toString());
        if (atDomain.endsWith("ibm.com")) {
            int breakhere = 10;
        }
        if (atDomain.endsWith("ge.com")) {
            int breakhere = 10;
        }

        String emailDomain = atDomain;
        int dot = atDomain.lastIndexOf('.');
        if (dot >= 0) {
            String tld = atDomain.substring(dot + 1);
            contactData.put(EMAILTOPLEVEL, tld);
            emailDomain = twoTld(atDomain);
            if (getCountryTLD().containsKey(tld)) {
                emailDomain = threeTld(atDomain);
            }
        }

        contactData.put(EMAILDOMAIN, emailDomain);
    }

    private String getAtDomain(String emailString) {
        int at = emailString.indexOf("@");
        if (at >= 0) {
            return emailString.substring(at + 1);
        }
        return emailString;
    }

    private String twoTld(String emailString) {
        int dot = emailString.lastIndexOf('.');
        int dot2 = emailString.lastIndexOf('.', dot - 1);
        if (dot2 < 0) {
            return emailString;
        }
        return emailString.substring(dot2 + 1);
    }

    private String threeTld(String emailString) {
        int dot = emailString.lastIndexOf('.');
        int dot2 = emailString.lastIndexOf('.', dot - 1);
        int dot3 = emailString.lastIndexOf('.', dot2 - 1);
        if (dot3 < 0) {
            return emailString;
        }
        return emailString.substring(dot3 + 1);
    }

    public String getEmailDomain() {
        return contactData.get(EMAILDOMAIN);
    }

    public String getSenorityLevel() {
        return contactData.get(SENIORITYLEVEL);
    }

    public String getSenoritySortLevel() {
        return contactData.get(SENIORITYSORTLEVEL);
    }

    private static HashMap<String, Integer> senoritySortMap = null;

    private static HashMap<String, Integer> getSenoritySortMap() {
        if (senoritySortMap == null) {
            senoritySortMap = new HashMap<>();
            senoritySortMap.put("Chief Information Officer", 10);
            senoritySortMap.put("Chief Technology Officer", 8);
            senoritySortMap.put("Executive VP", 6);
            senoritySortMap.put("Senior VP", 4);
            senoritySortMap.put("Group VP", 2);
            senoritySortMap.put("VP", 1);

            senoritySortMap.put("Executive Director", 22);
            senoritySortMap.put("Senior Director", 21);
            senoritySortMap.put("Director", 20);

            senoritySortMap.put("Senior Manager", 40);
            senoritySortMap.put("Manager", 45);
            senoritySortMap.put("Developer", 100);
            senoritySortMap.put("Engineer", 110);
            senoritySortMap.put("Team Lead", 111);
            senoritySortMap.put("Lead", 112);
            senoritySortMap.put("Programmer", 120);
        }
        return senoritySortMap;
    }

    public void createSenoritySort() {
        HashMap<String, Integer> senoritySortMap = Contact.getSenoritySortMap();
        String seniorityLevel = getSenorityLevel();

        if (!senoritySortMap.containsKey(seniorityLevel)) {
            throw new RuntimeException("Cannot find seniority sort level for " + seniorityLevel);
        }

        Integer senoritySortLevel = senoritySortMap.get(seniorityLevel);
        String senoritySortString = String.format("%08d", senoritySortLevel.intValue());

        contactData.put(SENIORITYSORTLEVEL, senoritySortString);
    }

    public static ArrayList<String> getAllHeaders() {
        return allHeaders;
    }

    public Object get(String header) {
        return contactData.get(header);
    }
}
