package com.ciber;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

    private static final String PATH = "/Users/markhahn/Ciber Global LLC/DevOps Practice Resources - Documents/Marketing/Campaign-2020-Q1/";

    private static final String FILE_NAME = "DevOps Lead Gen Campaign Target List Jan202.xlsx";
    private static final String xFILE_NAME = "DevOps Lead Gen Campaign Target List Short.xlsx";

    private static final String LEADGEN_FILE_NAME = "DevOps_LeadGen_";

    public static void main(String[] args) throws IOException {
        System.out.println("Starting");
        File inputFile = new File(PATH, FILE_NAME);

        FileInputStream excelFile = new FileInputStream(inputFile);
        Workbook workbook = new XSSFWorkbook(excelFile);

        Sheet dataSheet = workbook.getSheetAt(0);
        ArrayList<Contact> pnwContacts = extracted(dataSheet);
        System.out.println(pnwContacts.size());

        dataSheet = workbook.getSheetAt(1);
        ArrayList<Contact> swContacts = extracted(dataSheet);
        System.out.println(swContacts.size());

        dataSheet = workbook.getSheetAt(2);
        ArrayList<Contact> neContacts = extracted(dataSheet);
        System.out.println(neContacts.size());

        workbook.close();

        ArrayList<Contact> allContacts = new ArrayList<>();
        allContacts.addAll(pnwContacts);
        allContacts.addAll(swContacts);
        allContacts.addAll(neContacts);
        System.out.println(allContacts.size());

        int d = allContacts.size();
        int e = pnwContacts.size();
        int f = swContacts.size();
        int g = neContacts.size();

        System.out.printf("all vs pnw + sw + ne : %d = %d + %d + %d\n", d, e, f, g);

        if (d != (e + f + g)) {
            throw new RuntimeException("all count vs pwn + sw + ne counts");
        }

        HashMap<String, Company> companies = Company.sortCompanies(allContacts);
        System.out.printf("%d companies\n", companies.size());
        System.out.printf("%d contacts\n", Company.countContact(companies));

        HashMap<String, Company> sendContacts = new HashMap<>();
        HashMap<String, Company> holdContacts = new HashMap<>();

        Company.selectSendContact(sendContacts, holdContacts, companies);

        int a = countAllCompanyContacts(companies);
        int b = countAllCompanyContacts(sendContacts);
        int c = countAllCompanyContacts(holdContacts);

        System.out.printf("all vs send + hold : %d = %d + %d\n", a, b, c);

        if (a != (b + c)) {
            throw new RuntimeException("all count vs send + hold count");
        }

        LocalDate today = LocalDate.now();
        String dateString = today.toString();

        System.out.println(dateString);

        File sendFile = new File(PATH, LEADGEN_FILE_NAME + dateString + "_send.csv");
        writeContactListCSV(sendContacts, sendFile);
        File holdFile = new File(PATH, LEADGEN_FILE_NAME + dateString + "_hold.csv");
        writeContactListCSV(holdContacts, holdFile);

        System.out.println("Done.");
    }

    private static void writeContactListCSV(HashMap<String, Company> companyContactList, File outputFile) {

        System.out.println("Writing: " + outputFile);

        try {
            FileOutputStream outputStream = new FileOutputStream(outputFile);
            PrintStream ps = new PrintStream(outputStream);

            String separator = "";
            for (String header : Contact.getAllHeaders()) {
                ps.print(separator);
                ps.print(header.replaceAll(",", ""));
                separator = ",";
            }
            ps.println();

            int rowNum = 1;
            for (Company company : companyContactList.values()) {
                for (Contact contact : company.getContacts()) {
                    separator = "";
                    for (String header : Contact.getAllHeaders()) {
                        String fieldValue = "";
                        Object o = contact.get(header);
                        if (o != null) {
                            fieldValue = o.toString();
                            if (header.equals(Contact.EMAILDOMAIN)) {
                                fieldValue = " " + fieldValue;
                            }
                        }
                        ps.print(separator);
                        ps.print('"');
                        ps.print(fieldValue.replaceAll(",", ""));
                        ps.print('"');
                        separator = ",";
                    }
                    ps.println();
                    rowNum += 1;
                }
            }
            System.out.printf("Wrote %d rows\n", rowNum);

            ps.flush();
            ps.close();
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void writeContactList(HashMap<String, Company> companyContactList, String fileName) {

        System.out.println("Writing: " + fileName);

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Datatypes in Java");

        Row row = sheet.createRow(0);

        int colNum = 0;
        for (String header : Contact.getAllHeaders()) {
            Cell cell = row.createCell(colNum);
            cell.setCellValue(header);
            colNum += 1;
        }

        int rowNum = 1;
        for (Company company : companyContactList.values()) {
            for (Contact contact : company.getContacts()) {
                colNum = 0;
                Row contactRow = sheet.createRow(rowNum);
                for (String header : Contact.getAllHeaders()) {
                    Cell cell = contactRow.createCell(colNum);
                    Object o = contact.get(header);
                    if (o != null) {
                        String cellValue = o.toString();
                        cell.setCellValue(cellValue);
                    }
                    colNum += 1;
                }
                rowNum += 1;
            }
        }
        System.out.printf("Wrote %d rows\n", rowNum);

        try {
            FileOutputStream outputStream = new FileOutputStream(fileName);
            workbook.write(outputStream);
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static int countAllCompanyContacts(HashMap<String, Company> companies) {
        int count = 0;
        for (Company c : companies.values()) {
            count += c.countContacts();
        }
        return count;
    }

    private static ArrayList<Contact> extracted(Sheet dataSheet) {
        String sheetName = dataSheet.getSheetName();
        System.out.printf("Sheet name: %s\n", sheetName);
        int lastRowNum = dataSheet.getLastRowNum();
        System.out.printf("Last row number: %d\n", lastRowNum);

        List<String> headings = new ArrayList<>();

        int col = 0;
        boolean haveHeaders = true;
        boolean haveRD = true;
        Row headerRow = dataSheet.getRow(0);
        while (haveHeaders) {
            Cell headerCell = headerRow.getCell(col);
            if (headerCell == null) {
                haveHeaders = false;
            } else {
                if (headerCell.getCellType() == CellType.BLANK) {
                    haveHeaders = false;
                } else {
                    String header = headerCell.getStringCellValue();
                    headings.add(header);
                }
            }
            col += 1;
        }

        ArrayList<Contact> sheetContacts = new ArrayList<>();

        int rowNum = 1;
        while (haveRD) {
            Row row = dataSheet.getRow(rowNum);
            Contact rowContact = Contact.getContact(sheetName, headings, row);

            if (rowContact != null) {
                sheetContacts.add(rowContact);
            } else {
                haveRD = false;
            }

            if (rowNum == 1) {
                // rowContact.printMe();
            }

            rowNum += 1;
        }

        return sheetContacts;
    }

}
