package com.ciber;

import java.util.ArrayList;
import java.util.HashMap;

public class Company {

    ArrayList<Contact> companyContacts = new ArrayList<>();

    public static HashMap<String, Company> sortCompanies(ArrayList<Contact> allContacts) {

        createEmailDomain(allContacts);

        HashMap<String, Company> companies = new HashMap<>();
        for (Contact c : allContacts) {
            String emailDomain = c.getEmailDomain();
            if (companies.containsKey(emailDomain)) {
                Company company = companies.get(emailDomain);
                company.addContact(c);
            } else {
                Company company = new Company();
                company.addContact(c);
                companies.put(emailDomain, company);
            }
        }
        return companies;
    }

    public void addContact(Contact c) {
        companyContacts.add(c);

    }

    private static void createEmailDomain(ArrayList<Contact> allContacts) {
        for (Contact c : allContacts) {
            c.createEmailDomain();
        }

    }

    public static int countContact(HashMap<String, Company> companies) {
        int count = 0;
        for (String emailDomain : companies.keySet()) {
            Company company = companies.get(emailDomain);
            count += company.countContacts();
        }
        return count;
    }

    public int countContacts() {
        return companyContacts.size();
    }

    public static void selectSendContact(HashMap<String, Company> sendContacts, HashMap<String, Company> holdContacts,
            HashMap<String, Company> companies) {

        for (String emailDomain : companies.keySet()) {
            Company company = companies.get(emailDomain);

            if (emailDomain == null) {
                holdAllContacts(holdContacts, emailDomain, company);
            } else {
                if (emailDomain.isEmpty()) {
                    holdAllContacts(holdContacts, emailDomain, company);
                } else {
                    if (company.countContacts() <= 5) {
                        sendAllContacts(sendContacts, emailDomain, company);
                    } else {
                        selectSendContacts(sendContacts, holdContacts, emailDomain, company);
                    }
                }
            }
        }
    }

    private static void selectSendContacts(HashMap<String, Company> sendContacts, HashMap<String, Company> holdContacts,
            String emailDomain, Company company) {

        ArrayList<Contact> vpOrCLevel = new ArrayList<>();
        ArrayList<Contact> director = new ArrayList<>();
        ArrayList<Contact> manager = new ArrayList<>();
        ArrayList<Contact> individual = new ArrayList<>();

        for (Contact c : company.getContacts()) {
            c.createSenoritySort();
            String seniorityLevel = c.getSenorityLevel();
            switch (seniorityLevel) {
            case "Chief Information Officer":
            case "Executive VP":
            case "Senior VP":
            case "Group VP":
            case "Chief Technology Officer":
                vpOrCLevel.add(c);
                break;
            case "Executive Director":
            case "Director":
                director.add(c);
                break;
            case "Senior Manager":
            case "Manager":
                manager.add(c);
                break;
            case "Programmer":
            case "Engineer":
            case "Developer":
            case "Team Lead":
            case "Lead":
                individual.add(c);
                break;
            default:
                throw new RuntimeException("Cannot classify seniority level: " + seniorityLevel);
            }
        }

        int d = company.countContacts();
        int e = vpOrCLevel.size();
        int f = director.size();
        int g = manager.size();
        int h = individual.size();

        System.out.printf("%s: %d = %d + %d + %d + %d\n", emailDomain, d, e, f, g, h);
        if (d != (e + f + g + h)) {
            throw new RuntimeException("miss count on seniority level break out");
        }

        Company sendCompany = new Company();
        Company holdCompany = new Company();

        sortBySenorityLevel(vpOrCLevel);
        sortBySenorityLevel(director);
        sortBySenorityLevel(manager);

        int countVp = 0;
        for (Contact vpContact : vpOrCLevel) {
            if (countVp < 1) {
                sendCompany.addContact(vpContact);
                countVp += 1;
            } else {
                holdCompany.addContact(vpContact);
            }
        }

        int countDirector = 0;
        for (Contact directorConact : director) {
            if (countDirector < 1) {
                sendCompany.addContact(directorConact);
                countDirector += 1;
            } else {
                holdCompany.addContact(directorConact);
            }
        }

        for (Contact managerContact : manager) {
            if (sendCompany.countContacts() < 5) {
                sendCompany.addContact(managerContact);
            } else {
                holdCompany.addContact(managerContact);
            }
        }

        for (Contact individualContact : individual) {
            if (sendCompany.countContacts() < 5) {
                sendCompany.addContact(individualContact);
            } else {
                holdCompany.addContact(individualContact);
            }
        }

        sendContacts.put(emailDomain, sendCompany);
        holdContacts.put(emailDomain, holdCompany);

        int a = company.countContacts();
        int b = sendCompany.countContacts();
        int c = holdCompany.countContacts();

        System.out.printf("%s: %d = %d + %d\n", emailDomain, a, b, c);

        if (a != (b + c)) {
            throw new RuntimeException("miss count on send vs hold break out");
        }
    }

    private static void sortBySenorityLevel(ArrayList<Contact> vpOrCLevel) {

        // printLevels(vpOrCLevel);
        vpOrCLevel.sort((Contact c1, Contact c2) -> c1.getSenoritySortLevel().compareTo(c2.getSenoritySortLevel()));
        // printLevels(vpOrCLevel);
    }

    private static void printLevels(ArrayList<Contact> contactList) {

        for (Contact c : contactList) {
            System.out.print(c.getSenorityLevel());
            System.out.print("(");
            System.out.print(c.getSenoritySortLevel());
            System.out.print("), ");
        }
        System.out.println();
    }

    private static void sendAllContacts(HashMap<String, Company> sendContacts, String emailDomain, Company company) {
        Company sendCompany = new Company();
        for (Contact c : company.getContacts()) {
            sendCompany.addContact(c);
        }
        sendContacts.put(emailDomain, sendCompany);
    }

    private static void holdAllContacts(HashMap<String, Company> holdContacts, String emailDomain, Company company) {
        Company sendCompany = new Company();
        for (Contact c : company.getContacts()) {
            sendCompany.addContact(c);
        }
        holdContacts.put(emailDomain, sendCompany);

    }

    public Iterable<Contact> getContacts() {
        return companyContacts;
    }

}
